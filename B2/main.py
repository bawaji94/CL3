from flask import Flask, request

app=Flask(__name__)

ref=[]
filenames=open('files.txt').read().split('\n')
i=0
while i<len(filenames):
	f=open(filenames[i])
	content=f.read().split('\n')
	j=0
	while j<len(content):
		content[i]=content[i].strip()
		j=j+1
	ref.append(content)
	i=i+1

@app.route('/',methods=['GET','POST'])
def index():
	if request.method=="POST":
		value=request.form['plag']
		lines=value.split('\n')
		i=0
		while i<len(lines):
			if(len(lines[i].strip())==0):
				del lines[i]
				i=i-1
			i=i+1
		i=0
		count=0
		html='<html><body><a href="/"><button>Go back</button></a><br><p>The text entered was : </p>'
		while i<len(lines):
			j=0
			plager=False
			while j<len(ref):
				if lines[i].strip() in ref[j]:
					count=count+1
					plager=True
					html=html+'<b>'+lines[i]+'</b><br>'
					break

				if not plager:
					html=html+lines[i]+'<br>'
				j=j+1
			i=i+1
		per=((count*1.0)/len(lines))*100
		html=html+'<h2>Plagiarism : <h1>'+str(per)+'%</h1>'
		html=html+'<form action="/" method="POST"><textarea rows="10" cols="60" name="plag"></textarea><br><input type="submit" value="Submit" /></form></body></html>'
		return html
	else:
		html='<html><body><form action="/" method="POST"><textarea rows="10" cols="60" name="plag"></textarea><br><input type="submit" value="Submit" /></form></body></html>'
		return html

app.run()