import unittest

class Test(unittest.TestCase):
	pass

def test_generator(arr,target,actual):
	def test(self):
		self.assertEqual(BinarySearch(arr,target),actual)
	return test

def GetArray(string):
	string=string.split(',')
	j=0
	while j<len(string):
			string[j]=int(string[j])
			j=j+1
	return string


def getData(filename):
	fileContent=open(filename).read()
	lines=fileContent.split('\n')
	i=0
	testCases=[]
	while i<len(lines):
		row=lines[i].split(' ')
		row[0]=GetArray(row[0])
		row[1]=int(row[1])
		row[2]=int(row[2])
		testCases.append(row)
		i=i+1
	return testCases

def Sort(Input):
	Input.sort()
	return Input

def BinarySearch(array, target):
	array=Sort(array)

	left=0
	right=len(array)-1
	mid=(right+left)/2

	while(left <= right):
		if(array[mid]>target):
			right=mid-1
		elif(array[mid]<target):
			left=mid+1
		else:
			print "Got the element at position "+str(mid)
			return mid
		mid=(right+left)/2
	print "Element not present in the array"
	return -1

print "Enter 1 for Automated Testing"
print "Enter 2 for Taking array from file but maually entering the target number"
print "Enter 3 for Entering the array and target number manually"
input1=input()

if input1==1:
	print "Enter path to file :"
	fileName=raw_input()
	Data=getData(fileName)
	l=0
	while l<len(Data):
		test_name = 'test'+str(l)
		test = test_generator(Data[l][0],Data[l][1],Data[l][2])
		setattr(Test, test_name, test)
		l=l+1

	unittest.main()


elif input1==2:
	print "Enter path to file :"
	fileName=raw_input()
	Data=open(fileName).read()
	Data=GetArray(Data.split("\n")[0])
	print "Enter the number to find :"
	num=input()
	BinarySearch(Data,num)

elif input1==3:
	print "Enter the length of the array : "
	length=input()
	Data=[]
	print("Enter the array : ")
	l=0
	while l<length:
		temp=input()
		Data.append(temp)
		l=l+1
	print "Enter the number to find :"
	num=input()
	BinarySearch(Data,num)

else:
	print "Next time choose one of the above."