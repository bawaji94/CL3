import pymongo
import threading
import time

class Philosopher(threading.Thread):
	connection= pymongo.MongoClient('localhost',27017)
	# connection = pymongo.MongoClient('mongodb://username:password@localhost:27017')

	def __init__(self,i,name,l1,l2):
		threading.Thread.__init__(self)
		self.running=True
		self.fork1=l1
		self.fork2=l2
		self.index=i
		self.name=name

	def sendTo(self):
		col=Philosopher.connection.test.NewDini
		col.insert_one({"time":time.ctime(),"index":self.index})

	def eating(self):
		print self.name+" has started eating"
		self.sendTo()
		time.sleep(3)
		print self.name+" has finished eating"

	def grab(self):
		f1,f2=self.fork1,self.fork2
		while self.running:
			f1.acquire(True)
			got=f2.acquire(False)
			if got:
				break
			f1.release()
			f1,f2=f2,f1

		else:
			return

		self.eating()
		f2.release()
		f1.release()

	def run(self):
		while self.running:
			self.grab()

def main():
	forks=[]
	names=[]
	Philosophers=[]
	i=0
	while i<5:
		forks.append(threading.Lock())
		names.append("Philosopher"+str(i))
		i=i+1

	i=0
	while i<5:
		Philosophers.append(Philosopher(i,names[i],forks[i%5],forks[(i+1)%5]))
		i=i+1

	i=0
	while i<5:
		Philosophers[i].start()
		i=i+1

	i=0

	time.sleep(30)

	while i<5:
		Philosophers[i].running=False
		i=i+1

	while i<5:
		Philosophers[i].join()
		i=i+1

	print "We are done here"

main()