import java.text.DecimalFormat;
import java.util.*;

import org.cloudbus.cloudsim.*; 
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.lists.HostList;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple;



public class Hello {
	private static List<Cloudlet> cloudletlist;
	private static List<Vm> vmlist;
	
	public static void main(String[] args)
	{
		Log.printLine("Starting CloudSim");
		
		try
		{
			int num_user=1;
			Calendar calendar = Calendar.getInstance();
			boolean trace_flag = false;
			CloudSim.init(num_user, calendar, trace_flag);
			
			Datacenter datacenter = createDatacenter("Datacenter0");
			
			DatacenterBroker broker = createBroker();
			
			int brokerId = broker.getId();
			
			int vmid=0;
			int mips=1000;
			int pesNumber=1;
			int ram=512;
			int bw=1000;
			long size=10000;
			String vmm="Xen";
			
			
			Vm vm = new Vm(vmid, brokerId, mips,
					pesNumber, ram, bw,
					size, vmm,new CloudletSchedulerTimeShared());
			vmlist=new ArrayList<Vm>();
			vmlist.add(vm);
			broker.submitVmList(vmlist);
			cloudletlist = new ArrayList<Cloudlet>();
			
			int id=0;
			long cloudletLength=400000;
			long cloudletFileSize=300;
			long cloudletOutputSize=300;
			UtilizationModel utilizationmodel = new UtilizationModelFull();
		
			
			Cloudlet cloudlet = new Cloudlet(id, cloudletLength,
					pesNumber, cloudletFileSize, cloudletOutputSize, 
					utilizationmodel, utilizationmodel, utilizationmodel);
			
			cloudlet.setUserId(brokerId);
			cloudlet.setVmId(vmid);
			
			cloudletlist.add(cloudlet);
			
			broker.submitCloudletList(cloudletlist);
			
			CloudSim.startSimulation();
			
			CloudSim.stopSimulation();
			
			List<Cloudlet> newList = broker.getCloudletReceivedList();
			printCloudletList(newList);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	
	private static Datacenter createDatacenter(String name)
	{
		List<Host> hostList = new ArrayList<Host>();
		
		List<Pe> peList = new ArrayList<Pe>();
		
		int mips = 1000;
		
		peList.add(new Pe(0,new PeProvisionerSimple(mips)));
		
		int hostid=0;
		int ram = 2048;
		int bw=10000;
		long storage = 1000000;
		
		hostList.add(
				new Host(
						hostid,
						new RamProvisionerSimple(ram),
						new BwProvisionerSimple(bw),
						storage,
						peList,
						new VmSchedulerTimeShared(peList))
				);
		
		
		String arch = "x86";
		String os = "Linux";
		String vmm = "Xen";
		double timeZone = 10.0;
		double costPerSec=3.0;
		double costPerMem=0.05;
		double costPerStorage=0.0001;
		double costPerBw=0.0;
		
		DatacenterCharacteristics datacenterchar = new DatacenterCharacteristics
				(arch, os, vmm, 
				hostList, timeZone, costPerSec,
				costPerMem, costPerStorage, costPerBw);
		
		
		LinkedList<Storage> stoargeList = new LinkedList<Storage>();
		Datacenter datacenter = null;
		
		try
		{
			datacenter = new Datacenter(name, datacenterchar,
					new VmAllocationPolicySimple(hostList), stoargeList, 0);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return datacenter;
	}
	
	private static DatacenterBroker createBroker()
	{
		DatacenterBroker broker = null;
		
		try
		{
			broker=new DatacenterBroker("Broker");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return broker;
	}
	
	private static void printCloudletList(List<Cloudlet> list)
	{
		int size=list.size();
		String tab = "	  ";
		Cloudlet cloudlet;
		Log.printLine("-------------Output-------------");
		Log.printLine("CloudletID"+tab+"STATUS"+tab+"DataCenterID"
				+tab+"VM ID"+tab+"Time"+tab+"StartTime"+tab+"FinishTime");
		DecimalFormat dft = new DecimalFormat("###.##");
		for(int i=0;i<size;i++)
		{
			cloudlet = list.get(i);
			Log.print(cloudlet.getCloudletId()+tab+tab);
			if(cloudlet.getCloudletStatus()==Cloudlet.SUCCESS)
			{
				Log.print("SUCCESS");
				Log.printLine(tab+cloudlet.getResourceId()+tab+tab
						+cloudlet.getVmId()+tab+
						dft.format(cloudlet.getActualCPUTime())+
						tab+dft.format(cloudlet.getExecStartTime())+tab+tab+
						dft.format(cloudlet.getFinishTime()));
			}
		}
		
	}
	
}

