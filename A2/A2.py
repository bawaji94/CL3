import threading
import unittest
import xml.etree.ElementTree as et
from xml.dom import minidom

class Test(unittest.TestCase):
	pass

def test_generator(filename,actual):
	def test(self):
		self.assertEqual(func(filename),actual)

	return test

class Sort():
	arr=[]
	def __init__(self,a):
		self.arr=a

	def Partition(self,left,right):
		if right<left:
			return
		pivot=self.arr[left]
		i=left+1
		j=right
		while i<j:
			while self.arr[i]<pivot:
				i=i+1
			while self.arr[j]>pivot:
				j=j-1
			if i<j:
				temp=self.arr[i]
				self.arr[i]=self.arr[j]
				self.arr[j]=temp

		self.arr[left]=self.arr[j]
		self.arr[j]=pivot
		return j

	def QuickSort(self,left,right):
		if(left<right):
			mid=self.Partition(left,right)
			t1=threading.Thread(target=self.QuickSort,args=(left,mid-1))
			t2=threading.Thread(target=self.QuickSort,args=(mid+1,right))
			t1.start()
			t2.start()
			t1.join()
			print t1.getName()
			t2.join()
			print t2.getName()

			# self.QuickSort(left,mid-1)
			# self.QuickSort(mid+1,right)

def func(filename):
	try:
		name=filename.split(".")
		if name[-1]=="xml":
			tree=et.parse(filename)
			root=tree.getroot()
			a=map(int, root.text.split())
			o=Sort(a)
			newa=o.QuickSort(0,len(a)-1)
			print "sorted array is : " + str(o.arr) 
			return 'True'
		else:
			print "File must be XML"
	except Exception as e:
		print "Error check the file : "+filename
	return 'False'

xmldoc = minidom.parse('FileNames.xml')
Files = xmldoc.getElementsByTagName('File')
k=0
while k<len(Files):
	test=test_generator(str(Files[k].attributes['FileName'].value),str(Files[k].attributes['value'].value))
	setattr(Test,"test"+str(k),test)
	k=k+1
unittest.main()