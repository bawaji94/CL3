import java.net.*;
import java.io.*;

public class DServer1 extends Thread
{
   private ServerSocket serverSocket;
   
   public DServer1(int port) throws IOException
   {
      serverSocket = new ServerSocket(port);
   }

   public void run()
   {
      while(true)
      {
         try
         {
            System.out.println("Waiting for client on port " +
            serverSocket.getLocalPort() + "...");
            Socket server = serverSocket.accept();
            System.out.println("Just connected to "
                  + server.getRemoteSocketAddress());

	   BufferedReader br = new BufferedReader(new FileReader("/home/anuj/Documents/input1.txt"));
			System.out.println("Reading ...");
			String num2=br.readLine();

            DataOutputStream out = new DataOutputStream(server.getOutputStream());
            out.writeUTF(num2);
            server.close();

         }catch(SocketTimeoutException s)
         {
            System.out.println("Socket timed out!");
            break;
         }catch(IOException e)
         {
            e.printStackTrace();
            break;
         }
      }
   }
   public static void main(String [] args)
   {
      int port = 12345;
      try
      {
         Thread t = new DServer1(port);
         t.start();
      }catch(IOException e)
      {
         e.printStackTrace();
      }
   }
}