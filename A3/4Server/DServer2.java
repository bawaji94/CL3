import java.net.*;
import java.io.*;

public class DServer2 extends Thread
{
   private ServerSocket serverSocket;
   
   public DServer2(int port) throws IOException
   {
      serverSocket = new ServerSocket(port);
      
   }

   public void run()
   {
      while(true)
      {
         try
         {
            System.out.println("Waiting for client on port " +
            serverSocket.getLocalPort() + "...");
            Socket server = serverSocket.accept();
            System.out.println("Just connected to "
                  + server.getRemoteSocketAddress());

	   BufferedReader br = new BufferedReader(new FileReader("/home/anuj/Documents/input2.txt"));
			System.out.println("Reading ...");
			String num2=br.readLine();

            DataOutputStream out = new DataOutputStream(server.getOutputStream());
            out.writeUTF(num2);
            server.close();

         }catch(SocketTimeoutException s)
         {
            System.out.println("Socket timed out!");
            break;
         }catch(IOException e)
         {
            e.printStackTrace();
            break;
         }
      }
   }
   public static void main(String [] args)
   {
      int port = 5678;
      try
      {
         Thread t = new DServer2(port);
         t.start();
      }catch(IOException e)
      {
         e.printStackTrace();
      }
   }
}