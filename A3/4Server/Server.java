import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Server {
	   public static Scanner s = new Scanner(System.in);
	   
	  
	   public int BoothsMultiplier(int n1, int n2)
	    {
	        int[] m = binary(n1);
	        int[] m1 = binary(-n1);
	        int[] r = binary(n2);        
	        int[] A = new int[17];
	        int[] S = new int[17];
	        int[] P = new int[17];        
	        for (int i = 0; i < 8; i++)
	        {
	            A[i] = m[i];
	            S[i] = m1[i];
	            P[i + 8] = r[i];
	        }
		display(m, '1');
      		display(r, '2');
	        display(A, 'A');
	        display(S, 'S');
	        display(P, 'P');        
	        System.out.println();
	 
	        for (int i = 0; i < 8; i++)
	        {
	            if (P[15] == 0 && P[16] == 0);          
	            else if (P[15] == 1 && P[16] == 0)
	                add(P, S);                            
	            else if (P[15] == 0 && P[16] == 1)
	                add(P, A);            
	            else if (P[15] == 1 && P[16] == 1);
	 
	            rightShift(P);
	            display(P, 'P');
	        }
	        return getDecimal(P);
	    }
	   
	   public int getDecimal(int[] B)
	    {
	        int p = 0;
	        int t = 1;
	        for (int i = 15; i >= 0; i--, t *= 2)
	            p += (B[i] * t);
	        if (p > 15625)
	            p = -(65536 - p);
	        return p;        
	    }
	   
	   public void rightShift(int[] A)
	    {        
	        for (int i = 16; i >= 1; i--)
	            A[i] = A[i - 1];        
	    }
	   
	   public void add(int[] A, int[] B)
	    {
	        int carry = 0;
	        for (int i = 16; i >= 0; i--)
	        {
	            int temp = A[i] + B[i] + carry;
	            A[i] = temp % 2;
	            carry = temp / 2;
	        }        
	    }
	   
	   public int[] binary(int n)
	    {
	        int[] bin = new int[8];
	        int ctr = 7;
	        int num = n;
	        if (n < 0)
	            num = 256 + n;
	        while (num != 0)
	        {
	            bin[ctr--] = num % 2;
	            num /= 2;
	        }
	        return bin;
	    }
	   
	   public void display(int[] P, char ch)
	    { 
	        System.out.print("\n"+ ch +" : ");
	        for (int i = 0; i < P.length; i++)
	        {
	            if (i == 4)
	                System.out.print(" ");
	            if (i == 8)
	                System.out.print(" ");
		    if (i == 12)
	                System.out.print(" ");
	            System.out.print(P[i]);
	        } 
	    }

	      
	   public static void main(String argv[]) throws Exception

	     {

		boolean status=false;
	
		int somenum1=0;
		int somenum2=0;
		Server boothserver =new Server();

		  	  

	  	System.out.println(" Server is Running  " );

	        ServerSocket mysocket = new ServerSocket(2017);
	        mysocket.setReuseAddress(true);

	        while(true)

	        {

	           Socket connectionSocket = mysocket.accept();

	 	System.out.println("Connection done");
		   
	 	DataInputStream in= new DataInputStream(connectionSocket.getInputStream());
	
	 	
	 	String check=in.readUTF();
	 	System.out.println(check);
	 	//System.out.println("Data1 : "+data1);
		if(check.equals("Calculate"))
		{
			Socket client1 = new Socket("0.0.0.0", 12345);
			System.out.println("Just connected to " + client1.getRemoteSocketAddress());

			InputStream inFromServer1 = client1.getInputStream();
			DataInputStream in1 =new DataInputStream(inFromServer1);
			String n1=in1.readUTF();
			System.out.println("Server says " +n1);
			somenum1=Integer.parseInt(n1);

			Socket client2 = new Socket("0.0.0.0", 5678);
			System.out.println("Just connected to " + client2.getRemoteSocketAddress());

			InputStream inFromServer2 = client2.getInputStream();
			DataInputStream in2 =new DataInputStream(inFromServer2);
			String n2=in2.readUTF();
			System.out.println("Server says " +n2);
			somenum2=Integer.parseInt(n2);

			client1.close();
			client2.close();

			status=true;
	
		}
		else
		{
			System.out.println("no connection");
		}

		if(status==true)
		{
				   //BufferedWriter writer= new BufferedWriter(new OutputStreamWriter(connectionSocket.getOutputStream()));
				   
			
			
			OutputStream op = connectionSocket.getOutputStream();
			DataOutputStream op1= new DataOutputStream(op) ;
			
			
			op1.writeUTF("Booth Algorithm Assignment\n");            
				   op1.flush();

				   int result=boothserver.BoothsMultiplier(somenum1 , somenum2);          

				   System.out.println("Booth is done Displaying Result Below." );

				   op1.writeUTF("\r\n=== Result is  : "+result);

				 op1.flush();

				   connectionSocket.close();
		}

	        }//while

	     }//main

	}//main