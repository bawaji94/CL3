Run the from shell
`sudo python C2.py`

The server will start running on `localhost:5000`

Then go to web browser and the links are as follows with description : 

`/` - For creating Physical Volume, Logical Volume, Logical Partition

`/RemovePartition` - For removing Logical Partitions

`/RemoveVolume` - For removing Logical Volume

`/RemovePhysical` - For removing Physical Volume

`/RemoveAll` - To remove all Logical Partitions, Logical Volumes and Physical Volumes