import unittest
import json

class Test(unittest.TestCase):
	pass

def test_generator(name,actual):
	def test(self):
		self.assertEqual(func(name),actual)
	return test

def CanBePlaced(r,n,c):
	i=0
	while i<=n:
		if c==r[i]:
			return False
		if r[i]-i==c-n:
			return False
		if r[i]+i==c+n:
			return False
		i=i+1
	return True

def Queens(r,n):
	i=0
	while i<8:
		if(CanBePlaced(r,n,i)):
			r[n]=i
			if n==7:
				return True
			else:
				if(Queens(r,n+1)):
					return True
				else:
					r[n]=-1
		i=i+1
	return False

def PrintBoard(r):
	i=0
	while i<8:
		each=''
		j=0
		while j<8:
			if j==r[i]:
				each=each+'Q '
			else:
				each=each+'_ '
			j=j+1
		print each
		i=i+1



def func(filename):
	file=open(filename)
	data=json.load(file)
	rows=[]
	i=0
	while i<8:
		rows.append(-1)
		i=i+1

	if data['start']>=0 and data['start']<=7:
		rows[0]=data['start']
		if Queens(rows,1):
			PrintBoard(rows)
			return True
		else:
			print "Could not place all the queens with the initial settings"
	else:
		print "Invalid Input"
		return False

TestCases=json.load(open("FileNames.json"))
k=0
while k<len(TestCases):
	test=test_generator(TestCases[k]['FileName'],TestCases[k]['ActualValue'])
	setattr(Test,"test"+str(k),test)
	k=k+1
unittest.main()